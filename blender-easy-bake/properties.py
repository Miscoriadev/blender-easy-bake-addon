# Blender Easy Bake Add-on
# Contributor(s): Milan van Dijck (milan@miscoriadev.com)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import bpy

# A function returning a list of shaders for the cavity shader drop down list
def shader_items(self, context):
    return [(mat.name, mat.name, mat.name) for mat in bpy.data.materials]

class AUTOBAKER_LI_object(bpy.types.PropertyGroup):
    name: bpy.props.StringProperty(
        name="Name",
        description="Name of the object",
        default="Error"
    )
    lp: bpy.props.StringProperty(
        name="Low Poly",
        description="Low poly object",
        default="Error"
    )
    hp: bpy.props.StringProperty(
        name="High Poly",
        description="High poly object",
        default="Error"
    )
    cage: bpy.props.StringProperty(
        name="Cage",
        description="Cage object",
        default="Error"
    )

class AUTOBAKER_PGT_bake_props(bpy.types.PropertyGroup):
    LowPolyPostfix: bpy.props.StringProperty(name="Low poly postfix",
                                        description="The postfix of your low poly models. (If your model is named 'Orc_LP' enter the '_LP' part)",
                                        default="_LP",
                                        maxlen=1024,
                                        subtype="NONE")
    HighPolyPostfix: bpy.props.StringProperty(name="High poly postfix",
                                        description="The postfix of your high poly models. (If your model is named 'Orc_HP' enter the '_HP' part)",
                                        default="_HP",
                                        maxlen=1024,
                                        subtype="NONE")
    CagePostfix: bpy.props.StringProperty(name="Cage postfix",
                                        description="The postfix of your cage models. (If your model is named 'Orc_CAGE' enter the '_CAGE' part)",
                                        default="_CAGE",
                                        maxlen=1024,
                                        subtype="NONE")                                        
     
    TextureName: bpy.props.StringProperty(name="Texture name",
                                        description="The name of generated image textures, this name will be postfixed with the name of the pass (eg. generated_normal, generated_cavity)",
                                        default="generated",
                                        maxlen=1024,
                                        subtype="NONE")
    
    TextureSizeW: bpy.props.IntProperty(name="Texture width",
                                        description="The width of generated image textures",
                                        default=2048,
                                        subtype="PIXEL")
                                        
    TextureSizeH: bpy.props.IntProperty(name="Texture height",
                                        description="The height of generated image textures",
                                        default=2048,
                                        subtype="PIXEL")
                                        
    RenderCavity: bpy.props.BoolProperty(name="Cavity map",
                                        description="Whether to render the cavity map for the selected model",
                                        default=False)
                                        
    RenderNormal: bpy.props.BoolProperty(name="Normal map",
                                        description="Whether to render the normal map for the selected model",
                                        default=False)
                                        
    RenderAOC: bpy.props.BoolProperty(name="Ambient occlusion map",
                                        description="Whether to render the ambient occlusion map for the selected model",
                                        default=False)
                                                                  
    CavityShader: bpy.props.EnumProperty(name="Cavity shader",
                                        items=shader_items, 
                                        description="Select the shader you want to use for you cavity map")
    
    Objects: bpy.props.CollectionProperty(
        type=AUTOBAKER_LI_object,
        description="Custom render passes that can be output by shader nodes",
    )
    
    ActiveObject: bpy.props.IntProperty(
        default=0,
        min=0
    )

classes = [
    AUTOBAKER_LI_object,
    AUTOBAKER_PGT_bake_props
]

def register():
    for cls in classes:
        bpy.utils.register_class(cls)
    bpy.types.Scene.AutoBakerProps = bpy.props.PointerProperty(type=AUTOBAKER_PGT_bake_props)

def unregister():
    for cls in classes:
        bpy.utils.unregister_class(cls)
    del bpy.types.Scene.AutoBakerProps