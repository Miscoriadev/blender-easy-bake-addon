# Blender Easy Bake Add-on
# Contributor(s): Milan van Dijck (milan@miscoriadev.com)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

bl_info = {
        "name": "Easy Bake",
        "description": "Easy bake allows you easily bake normal, ambient occlusion, and cavity maps from high to low poly objects.",
        "author": "Milan van Dijck",
        "version": (1, 0),
        "blender": (3, 0, 0),
        "location": "Properties > Render > Easy bake",
        "warning": "", # used for warning icon and text in add-ons panel
        "wiki_url": "https://gitlab.com/Miscoriadev/blender-easy-bake-addon/-/wikis/home",
        "tracker_url": "https://gitlab.com/Miscoriadev/blender-easy-bake-addon/-/issues",
        "support": "COMMUNITY",
        "category": "Render"
        }

import bpy

#
# Add additional functions here
#

def register():
    from . import properties
    from . import operators
    from . import ui
    properties.register()
    operators.register()
    ui.register()

def unregister():
    from . import properties
    from . import operators
    from . import ui
    properties.unregister()
    operators.unregister()
    ui.unregister()

if __name__ == '__main__':
    register()