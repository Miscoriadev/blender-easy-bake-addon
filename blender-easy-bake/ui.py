# Blender Easy Bake Add-on
# Contributor(s): Milan van Dijck (milan@miscoriadev.com)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import bpy
import re
from time import sleep

from bpy.types import Panel, Operator, PropertyGroup

# A function to assign materials to object
def assignMaterial(ob, materialName):
    # Get material
    mat = bpy.data.materials.get(materialName)
    if mat is None:
        # create material
        mat = bpy.data.materials.new(name=materialName)

    # Assign it to object
    if ob.data.materials:
        # assign to 1st material slot
        ob.data.materials[0] = mat
    else:
        # no slots
        ob.data.materials.append(mat)

# Object for rendering list items 
class AUTOBAKER_UL_objects(bpy.types.UIList):
    def draw_item(self, context, layout, data, item, icon, active_data, active_propname):
        row = layout.row()
        split = row.split(factor=0.65)
        icon = 'MESH_CUBE'
        split.row().prop(item, "name", text="", icon=icon, emboss=False)
 
 # The main UI panel
class AUTOBAKER_PT_render_easybake(Panel):
    bl_label = "Easy bake"
    bl_region_type = "WINDOW"
    bl_context = "render"
    bl_space_type = 'PROPERTIES'
    COMPAT_ENGINES = {'CYCLES'}
    bl_idname = "AUTOBAKER_PT_render_easybake"
 
    def draw(self, context):
        
        autoBakerProps = context.scene.AutoBakerProps        
        layout = self.layout
        
        box= layout.box()
        
        box.label(text="Textures to generate", icon='RENDER_RESULT')
        
        row = box.row()
        row.prop(autoBakerProps, "RenderNormal")
        
        row = box.row()
        row.prop(autoBakerProps, "RenderAOC")
        
        row = box.row()
        col = row.column()
        col.prop(autoBakerProps, "RenderCavity")
        col = row.column()
        col.enabled = autoBakerProps.RenderCavity
        col.prop(autoBakerProps, "CavityShader")
        
        row = layout.row()
        row.template_list(
            "AUTOBAKER_UL_objects",
            "objects_to_bake",
            autoBakerProps,
            "Objects",
            autoBakerProps,
            "ActiveObject",
            rows=5,
        )
        
        col = row.column(align=True)
        col.operator("autobaker.search_op", icon='UV_SYNC_SELECT', text="")
        
        row = layout.row()
        row.enabled = autoBakerProps.RenderCavity or autoBakerProps.RenderNormal or autoBakerProps.RenderAOC
        row.operator("autobaker.bake_op") 
        
        #box = layout.box()
        #row = box.row()
        #ow.label(text="Optional bake settings", icon='SCENE')
        
        #row = box.row()
        #row.prop(context.scene.render.bake, "cage_extrusion")
        
        #row = box.row()
        #row.prop(context.scene.render.bake, "max_ray_distance")
        
        #row = box.row()
        #row.prop(context.scene.render.bake, "margin")

class AUTOBAKER_PT_render_easybake_naming(Panel):
    bl_label = "Object naming"
    bl_context = "render"
    bl_space_type = 'PROPERTIES'
    bl_region_type = "WINDOW"
    bl_parent_id = "AUTOBAKER_PT_render_easybake"
    COMPAT_ENGINES = {'CYCLES'}

    def draw_header(self, context):
        self.layout.label(text="", icon='SCENE_DATA')

    def draw(self, context):
        autoBakerProps = context.scene.AutoBakerProps

        layout = self.layout
        
        #row = layout.row()
        #row.label(text="Object naming", icon='SCENE_DATA')
        
        row = layout.row()
        row.prop(autoBakerProps, "LowPolyPostfix")
        
        row = layout.row()
        row.prop(autoBakerProps, "HighPolyPostfix")
        
        row = layout.row()
        row.prop(autoBakerProps, "CagePostfix")

class AUTOBAKER_PT_render_easybake_textures(Panel):
    bl_label = "Generated Texture"
    bl_context = "render"
    bl_space_type = 'PROPERTIES'
    bl_region_type = "WINDOW"
    bl_parent_id = "AUTOBAKER_PT_render_easybake"
    COMPAT_ENGINES = {'CYCLES'}

    def draw_header(self, context):
        self.layout.label(text="", icon='OUTLINER_OB_IMAGE')

    def draw(self, context):
        autoBakerProps = context.scene.AutoBakerProps

        layout = self.layout
        
        #row = layout.row()
        #row.label(text="Generated Texture", icon='OUTLINER_OB_IMAGE')
        
        row = layout.row()
        row.prop(autoBakerProps, "TextureName")    
        row = layout.row()
        col = row.column()
        col.prop(autoBakerProps, "TextureSizeW")
        col = row.column()
        row.prop(autoBakerProps, "TextureSizeH") 

classes = [
    AUTOBAKER_UL_objects,
    AUTOBAKER_PT_render_easybake,
    AUTOBAKER_PT_render_easybake_naming,
    AUTOBAKER_PT_render_easybake_textures
]
 
 
def register():
    for cls in classes:
        bpy.utils.register_class(cls)

        
 
def unregister():
    for cls in classes:
        bpy.utils.unregister_class(cls)
