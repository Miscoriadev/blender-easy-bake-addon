# Blender Easy Bake Add-on
# Contributor(s): Milan van Dijck (milan@miscoriadev.com)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import bpy
import re

from bpy.types import Operator

############ Fix for UI freezing during bake ############
# https://devtalk.blender.org/t/question-about-ui-lock-ups-when-running-a-python-script/6406/6

# A function to assign materials to object
def assignMaterial(ob, materialName):
    # Get material
    mat = bpy.data.materials.get(materialName)
    if mat is None:
        # create material
        mat = bpy.data.materials.new(name=materialName)

    # Assign it to object
    if ob.data.materials:
        # assign to 1st material slot
        ob.data.materials[0] = mat
    else:
        # no slots
        ob.data.materials.append(mat)

class AUTOBAKER_OT_search_objects(Operator):
    """Search objects in the scene that comply with the object naming scheme"""
    bl_label = "Search for objects"
    bl_idname = "autobaker.search_op"
    
    def execute(self, context):
        # Remove existing objects
        context.scene.AutoBakerProps.Objects.clear()
            
        # A dictionary to hold all objects
        object_dict = {}
        
        # Fill the dictionary with the components
        lp_regex = re.compile("^(.*)%s$" % (context.scene.AutoBakerProps.LowPolyPostfix))
        hp_regex = re.compile("^(.*)%s$" % (context.scene.AutoBakerProps.HighPolyPostfix))
        cage_regex = re.compile("^(.*)%s$" % (context.scene.AutoBakerProps.CagePostfix))
        
        for ob in bpy.data.objects:
            # search for low poly models
            lp_search = lp_regex.search(ob.name)
            if lp_search:
                if lp_search.group(1) in object_dict:
                    object_dict[lp_search.group(1)]['lp'] = ob.name
                else:
                    object_dict[lp_search.group(1)] = {}
                    object_dict[lp_search.group(1)]['lp'] = ob.name
            
            # search for high poly models
            hp_search = hp_regex.search(ob.name)
            if hp_search:
                if hp_search.group(1) in object_dict:
                    object_dict[hp_search.group(1)]['hp'] = ob.name
                else:
                    object_dict[hp_search.group(1)] = {}
                    object_dict[hp_search.group(1)]['hp'] = ob.name
            
            # search for cage models
            cage_search = cage_regex.search(ob.name)
            if cage_search:
                if cage_search.group(1) in object_dict:
                    object_dict[cage_search.group(1)]['cage'] = ob.name
                else:
                    object_dict[cage_search.group(1)] = {}
                    object_dict[cage_search.group(1)]['cage'] = ob.name
        
        for key, value in object_dict.items():
            if 'lp' in value and 'hp' in value and 'cage' in value:
                my_item = bpy.context.scene.AutoBakerProps.Objects.add()
                my_item.name = key
                my_item.lp = value['lp']
                my_item.hp = value['hp']
                my_item.cage = value['cage']
        
        return {'FINISHED'}
    
class AUTOBAKER_OT_bake_button(Operator):
    """Get out the cookie sheets! We're bakin!"""
    bl_label = "Bake!"
    bl_idname = "autobaker.bake_op"
    
    def execute(self, context):
        settings = context.scene.AutoBakerProps
        
        if (len(settings.Objects.keys()) > 0):
            # Unhide all collections
            for c in bpy.context.view_layer.layer_collection.children:
                c.hide_viewport=False
                c.collection.hide_render=False
                #c.hide_set(False)
    
            # Get the selected model
            object_list_item = settings.Objects[settings.ActiveObject]
            
            # Get all objects from the selected model
            lp_object = bpy.data.objects[object_list_item.lp]
            hp_object = bpy.data.objects[object_list_item.hp]
            
            # Unhide objects
            lp_object.hide_set(False)
            lp_object.hide_render=False
            hp_object.hide_set(False)
            hp_object.hide_render=False
            
            # Deselect any currently selected models
            for obj in bpy.data.objects:
                obj.select_set(False)
            
            # Select the lp and hp models
            lp_object.select_set(True)
            hp_object.select_set(True)
            
            # active select low poly object
            bpy.context.view_layer.objects.active = lp_object
            
            # Set smooth shading for both lp and hp objects
            bpy.ops.object.shade_smooth()
            for poly in lp_object.data.polygons:
                poly.use_smooth = True
            
            for poly in hp_object.data.polygons:
                poly.use_smooth = True


            # Get low poly object material
            material = lp_object.active_material
            
            # if the material does not exist, create it
            if (material):
                pass
            else:
                material = bpy.data.materials.get(settings.TextureName)
                if material is None:
                    material = bpy.data.materials.new(name=settings.TextureName)
            
            material.use_nodes = True
            nodes = material.node_tree.nodes

            # Add an image texture node and set its location
            tex = nodes.new("ShaderNodeTexImage")
            tex.name = "Automatic baking node"
            tex.location = (0,0)
            
            # Make sure the node is selected
            tex.select = True
            nodes.active = tex
            
            self.report({"INFO"}, "Starting bake...")
            
            # If normal texture rendering is enabled execute it
            if (settings.RenderNormal):
                
                # check if the target image block exists, if not, create it
                if (bpy.data.images.get(settings.TextureName+"_normal")):
                    img = bpy.data.images.get(settings.TextureName+"_normal")
                else:
                    img = bpy.data.images.new(settings.TextureName+"_normal", settings.TextureSizeW, settings.TextureSizeH)
                
                # Add the image datablock we want to render to, to the texture node  
                tex.image = img
                
                bpy.ops.object.bake( 
                    type='NORMAL', 
                    margin=bpy.context.scene.render.bake.margin, 
                    use_selected_to_active=True,
                    use_cage=True,
                    use_clear=False,
                    target='IMAGE_TEXTURES',
                    save_mode='INTERNAL',
                    cage_object=object_list_item.cage
                )
                
            # If normal texture rendering is enabled execute it
            if (settings.RenderCavity):
                
                # check if the target image block exists, if not, create it
                if (bpy.data.images.get(settings.TextureName+"_cavity")):
                    img = bpy.data.images.get(settings.TextureName+"_cavity")
                else:
                    img = bpy.data.images.new(settings.TextureName+"_cavity", settings.TextureSizeW, settings.TextureSizeH)
                
                # Add the image datablock we want to render to, to the texture node  
                tex.image = img
                
                # Force the cavity shader for the hp object
                assignMaterial(hp_object, settings.CavityShader)
                
                bpy.ops.object.bake( 
                    type='DIFFUSE',
                    pass_filter={'COLOR'},
                    margin=bpy.context.scene.render.bake.margin, 
                    use_selected_to_active=True,
                    use_cage=True,
                    use_clear=False,
                    target='IMAGE_TEXTURES',
                    save_mode='INTERNAL',
                    cage_object=object_list_item.cage
                )
            
            # If aoc texture rendering is enabled execute it
            if (settings.RenderAOC):
                
                # check if the target image block exists, if not, create it
                if (bpy.data.images.get(settings.TextureName+"_ao")):
                    img = bpy.data.images.get(settings.TextureName+"_ao")
                else:
                    img = bpy.data.images.new(settings.TextureName+"_ao", settings.TextureSizeW, settings.TextureSizeH)
                
                # Add the image datablock we want to render to, to the texture node  
                tex.image = img
                
                # We need to make sure that all cage objects are hidden during ambient occlusion render
                for object in settings.Objects:
                    generic_cage_object = bpy.data.objects[object.cage]
                    generic_cage_object.hide_set(True)
                    generic_cage_object.hide_render=True
                
                bpy.ops.object.bake( 
                    type='AO',
                    margin=bpy.context.scene.render.bake.margin, 
                    use_selected_to_active=True,
                    use_cage=True,
                    use_clear=False,
                    target='IMAGE_TEXTURES',
                    save_mode='INTERNAL',
                    cage_object=object_list_item.cage
                )
                
                # We need to make sure that all cage objects are unhidden when we are done rendering
                for object in settings.Objects:
                    generic_cage_object = bpy.data.objects[object.cage]
                    generic_cage_object.hide_set(False)
                    generic_cage_object.hide_render=False
                
            ###### cleanup ######
            
            # remove the texture node
            material.node_tree.nodes.remove(tex)
                    
            self.report({"INFO"}, "Bake complete!")
        else:
            self.report({"ERROR"}, "No object to bake selected")
        
        return {'FINISHED'}
 
classes = [
    AUTOBAKER_OT_search_objects,
    AUTOBAKER_OT_bake_button
]
 
 
def register():
    for cls in classes:
        bpy.utils.register_class(cls)

 
def unregister():
    for cls in classes:
        bpy.utils.unregister_class(cls)
